package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.ArrayList;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		domController.dom();
		// sort (case 1)
		// PLACE YOUR CODE HERE
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		DOMxmlWriter doMxmlWriter1 = new DOMxmlWriter(domController.flowers);

		DOMxmlWriter.main(new String []{outputXmlFile});
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		parser.parse(new File(xmlFileName), saxController);
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		DOMxmlWriter doMxmlWriter = new DOMxmlWriter(saxController.flowers);
		doMxmlWriter.main(new String[]{outputXmlFile});
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		ArrayList<Flowers> list = staxController.parseXMLfile();
		DOMxmlWriter doMxmlWriter2 = new DOMxmlWriter(list);
		doMxmlWriter2.main(new String[]{outputXmlFile});
	}

}
