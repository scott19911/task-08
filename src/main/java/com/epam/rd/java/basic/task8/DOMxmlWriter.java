package com.epam.rd.java.basic.task8;
import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class DOMxmlWriter {
    public DOMxmlWriter(ArrayList<Flowers> flow) {
        this.flow = flow;
    }

    static ArrayList<Flowers> flow;

    public static void main(String[] args) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        String path = args[0];
        try {
            builder = factory.newDocumentBuilder();

            // создаем пустой объект Document, в котором будем
            // создавать наш xml-файл
            Document doc = builder.newDocument();
            // создаем корневой элемент
            Element rootElement = doc.createElement("flowers");
            rootElement.setAttribute("xmlns", "http://www.nure.ua");
            rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
            // добавляем корневой элемент в объект Document
            doc.appendChild(rootElement);
            for (Flowers flowers: flow
                 ) {
                // добавляем первый дочерний элемент к корневому
                Element flower = doc.createElement("flower");
                rootElement.appendChild(flower);
                flower.appendChild(getFlowerElements(doc, flower, "name", flowers.getName()));
                flower.appendChild(getFlowerElements(doc, flower, "soil", flowers.getSoil()));
                flower.appendChild(getFlowerElements(doc, flower, "origin", flowers.getOrigin()));

                Element visualParameters = doc.createElement("visualParameters");
                flower.appendChild(visualParameters);
                visualParameters.appendChild(getFlowerElements(doc, visualParameters, "stemColour", flowers.getStemColor()));
                visualParameters.appendChild(getFlowerElements(doc, visualParameters, "leafColour", flowers.getLeafColour()));

                Element ave = doc.createElement("aveLenFlower");
                ave.setAttribute("measure", "cm");
                ave.appendChild(doc.createTextNode(String.valueOf(flowers.getAveLenFlower())));
                visualParameters.appendChild(ave);

                Element growingTips = doc.createElement("growingTips");
                flower.appendChild(growingTips);

                Element tempreture = doc.createElement("tempreture");
                tempreture.setAttribute("measure", "celcius");
                tempreture.appendChild(doc.createTextNode(String.valueOf(flowers.getTempreture())));
                growingTips.appendChild(tempreture);

                Element lighting = doc.createElement("lighting");
                lighting.setAttribute("lightRequiring", flowers.getLighting());
                growingTips.appendChild(lighting);

                Element watering = doc.createElement("watering");
                watering.setAttribute("measure", "mlPerWeek");
                watering.appendChild(doc.createTextNode(String.valueOf(flowers.getWatering())));
                growingTips.appendChild(watering);
                Element multiplying = doc.createElement("multiplying");
                flower.appendChild(getFlowerElements(doc, multiplying, "multiplying", flowers.getMultiplying()));
            }
            //добавляем второй дочерний элемент к корневому


            //создаем объект TransformerFactory для печати в консоль
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            // для красивого вывода в консоль
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);

            //печатаем в консоль или файл
            StreamResult console = new StreamResult(System.out);
            StreamResult file = new StreamResult(new File(path));

            //записываем данные
            transformer.transform(source, console);
            transformer.transform(source, file);
            System.out.println("Создание XML файла закончено " + path);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // метод для создания нового узла XML-файла
    private static Node getFlower(Document doc,  String name, String age) {
        Element flower = doc.createElement("Flower");



        // создаем элемент name
        flower.appendChild(getFlowerElements(doc, flower, "name", name));

        // создаем элемент age
        flower.appendChild(getFlowerElements(doc, flower, "age", age));
        return flower;
    }


    // утилитный метод для создание нового узла XML-файла
    private static Node getFlowerElements(Document doc, Element element, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }

}