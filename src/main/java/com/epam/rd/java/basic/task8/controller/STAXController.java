package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flowers;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private  String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}


	public  ArrayList<Flowers> parseXMLfile() {
		ArrayList<Flowers> flowersList = new ArrayList<>();
		Flowers flower = null;
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			// инициализируем reader и скармливаем ему xml файл
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			// проходим по всем элементам xml файла
			while (reader.hasNext()) {
				// получаем событие (элемент) и разбираем его по атрибутам
				XMLEvent xmlEvent = reader.nextEvent();
				if (xmlEvent.isStartElement()) {
					StartElement startElement = xmlEvent.asStartElement();
					switch (startElement.getName().getLocalPart()){
						case "flower" : flower = new Flowers();
						break;
						case "name":
							xmlEvent = reader.nextEvent();
							if (flower != null) {
								flower.setName(xmlEvent.asCharacters().getData());
							}
							break;
						case "soil":xmlEvent = reader.nextEvent();
							if (flower != null) {
								flower.setSoil(xmlEvent.asCharacters().getData());
							}
							break;
						case "origin":xmlEvent = reader.nextEvent();
							if (flower != null) {
								flower.setOrigin(xmlEvent.asCharacters().getData());
							}
							break;
						case "stemColour":xmlEvent = reader.nextEvent();
							if (flower != null) {
								flower.setStemColor(xmlEvent.asCharacters().getData());
							}
							break;
						case "leafColour":xmlEvent = reader.nextEvent();
							if (flower != null) {
								flower.setLeafColour(xmlEvent.asCharacters().getData());
							}
							break;
						case "aveLenFlower":xmlEvent = reader.nextEvent();
							if (flower != null) {
								flower.setAveLenFlower(Integer.parseInt(xmlEvent.asCharacters().getData()));
							}
							break;
						case "tempreture":xmlEvent = reader.nextEvent();
							if (flower != null) {
								flower.setTempreture(Integer.parseInt(xmlEvent.asCharacters().getData()));
							}
							break;
						case "lighting":xmlEvent = reader.nextEvent();
							if (flower != null) {
								flower.setLighting(String.valueOf(startElement.getAttributeByName(new QName("lightRequiring")).getValue()));
							}
							break;
						case "watering":xmlEvent = reader.nextEvent();
							if (flower != null) {
								flower.setWatering(Integer.parseInt(xmlEvent.asCharacters().getData()));
							}
							break;
						case "multiplying":xmlEvent = reader.nextEvent();
							if (flower != null) {
								flower.setMultiplying(xmlEvent.asCharacters().getData());
							}
							break;
					}
				}

				// если цикл дошел до закрывающего элемента,
				// то добавляем считанного из файла  в список
				if (xmlEvent.isEndElement()) {
					EndElement endElement = xmlEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("flower")) {
						flowersList.add(flower);
					}
				}
			}

		} catch (FileNotFoundException | XMLStreamException exc) {
			exc.printStackTrace();
		}

		return flowersList;
	}

}