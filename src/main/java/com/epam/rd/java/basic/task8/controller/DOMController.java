package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flowers;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;

/**
 * Controller for DOM parser.
 */
public class DOMController {



	public DOMController(String xmlFileName)  {
		this.xmlFileName = xmlFileName;
	}
	private static String xmlFileName;
	static String name;
	static String origin;
	static String soil;
	static String stemColor;
	static String  multiplying;
	static String leafColour;
	static int aveLenFlower;
	static int tempreture;
	static String lighting;
	static int watering;
	public static ArrayList<Flowers> flowers = new ArrayList<>();

	// PLACE YOUR CODE HERE
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	DocumentBuilder builder;
	// Получили из фабрики билдер, который парсит XML, создает структуру Document в виде иерархического дерева.

	public void dom() {

		//File xmlFile = new File(xmlFileName);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(xmlFileName);
			doc.getDocumentElement().normalize();

			// получаем узлы с именем Language
			// теперь XML полностью загружен в память
			// в виде объекта Document
			NodeList nList = doc.getElementsByTagName("flower");

			// создадим из него список объектов Language

			for (int i = 0; i < nList.getLength(); i++) {
				Node nNode = nList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
							name = eElement.getElementsByTagName("name").item(0)
							.getTextContent();
					soil=eElement.getElementsByTagName("soil")
							.item(0)
							.getTextContent();
					origin =eElement.getElementsByTagName("origin")
							.item(0)
							.getTextContent();

					stemColor =eElement.getElementsByTagName("stemColour")
							.item(0)
							.getTextContent();

					leafColour =eElement.getElementsByTagName("leafColour")
							.item(0)
							.getTextContent();

					aveLenFlower = Integer.parseInt(eElement.getElementsByTagName("aveLenFlower")
							.item(0)
							.getTextContent());

					tempreture = Integer.parseInt(eElement.getElementsByTagName("tempreture")
							.item(0)
							.getTextContent());

					Element temp =(Element)  eElement.getElementsByTagName("lighting").item(0);
					lighting = temp.getAttribute("lightRequiring");

					watering = Integer.parseInt(eElement.getElementsByTagName("watering")
							.item(0)
							.getTextContent());

					multiplying = eElement.getElementsByTagName("multiplying")
							.item(0)
							.getTextContent();

				flowers.add(new Flowers(name, soil,origin,  stemColor, leafColour,  aveLenFlower, tempreture,
					 lighting,  watering,  multiplying));
				}

			}

			// печатаем в консоль информацию по каждому объекту Language

		} catch (Exception exc) {
			exc.printStackTrace();
		}

	}




}

