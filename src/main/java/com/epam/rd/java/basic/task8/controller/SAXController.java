package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flowers;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName, lastElementName;
	String name;
	String origin;
	String soil;
	String stemColor;
	String  multiplying;
	String leafColour;
	int aveLenFlower;
	int tempreture;
	String lighting;
	int watering;
	public ArrayList<Flowers> flowers = new ArrayList<>();


	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (!qName.isBlank()) {
			lastElementName = qName;
			//System.out.println(lastElementName);
			if (qName.equals("lighting")){
				lighting = attributes.getValue("lightRequiring");
			}
		}
	}
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String information = new String(ch, start, length);

		information = information.replace("\n", "").trim();

		if( !information.isEmpty()){

			if (lastElementName.equals("name")) {
				 name = information;}
			if (lastElementName.equals("soil")) {
				soil = information;}
			switch (lastElementName){
				case  "origin" : origin = information;
				break;
				case  "stemColour" : stemColor = information;
				break;case  "leafColour" : leafColour = information;
				break;case  "aveLenFlower" : aveLenFlower = Integer.parseInt(information);
				break;case  "tempreture" : tempreture = Integer.parseInt(information);
				break;case  "watering" : watering = Integer.parseInt(information);
				break;case  "multiplying" :multiplying = information; break;

			}


		}

	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if ( name != null &&  soil != null && origin != null  && stemColor!=null &&
		leafColour != null && aveLenFlower !=0 && tempreture !=0 &&
		lighting !=null && watering !=0 &&multiplying !=null) {
			flowers.add(new Flowers(name, soil, origin, stemColor, leafColour, aveLenFlower, tempreture, lighting, watering, multiplying));
		name = null;soil = null;stemColor = null;
		origin = null;
		leafColour = null;aveLenFlower = 0;tempreture = 0;lighting = null;watering = 0; multiplying = null;
	}}


}
	// PLACE YOUR CODE HERE

